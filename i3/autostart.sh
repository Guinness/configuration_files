#!/bin/bash
# -*- coding: UTF8 -*-

nm-applet &
blueman-applet &
amixer -q sset Master 30%
amixer -q sset Master mute
xset -b &
dunst &
xset dpms 300 600 900 &
xbacklight -set 17 &
setxkbmap -option caps:swapescape
